import * as dotenv from 'dotenv'
dotenv.config();
import Express from "express";
import cors from "cors";
import session from "express-session";
import db from "./config/database.js"; //integrasi db
import SequelizeStore from "connect-session-sequelize";
import UserRoute from "./routes/UserRoute.js";
import ProductRoute from "./routes/ProductRoute.js";
import AuthRoute from "./routes/AuthRoute.js";

const port = process.env.APP_PORT;

const app = Express();

// --------------------------------------------------
//S001: untuk save session ke db
const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
    db: db
});
// end
// ----------------------------------------------------

// (async() => {
//     await db.sync({
//         alter:true
//     });
// })();

app.use(session({
    secret: process.env.SESS_SECRET,
    resave: false,
    // ----------------
    //S002: jalankan S001 terlebih dahulu
    store: store,
    // ------------------
    saveUninitialized: true,
    cookie: {
        secure: false //set false jika http, true jika https, auto otomatis search
    },
}));

app.use(cors({
    credentials: true, //frontend bisa memberikan request beserta cookie menyertakan credential
    origin: ['http://localhost:3000'],//domain yang diizinkan untuk mengakses API kita
})); 
app.use(Express.json()); //agar kita bisa menerima data dalam format json

//setting middleware
app.use(UserRoute); //use rote nya ke middleware
app.use(ProductRoute); //suse rote nya ke middleware
app.use(AuthRoute); //use rote nya ke middleware

// -----------------------
// S003: buat session table di db, jalankan setelah S002
///**** store.sync();
// nonaktifkan setelah buat table session
// -------------------------
app.listen(process.env.APP_PORT, ()=> {
    console.log('Server up and running...');
});