import User from "../models/UserModel.js";

//kita tinggal memanggil fungsi dibawah untuk filter middleware nya di route
export const verifyUser = async(req, res, next) => {
    if (!req.session.userId) {
        return res.status(401).json({msg: "no account login right now"});
    }
    const findUser = await User.findOne({
        where: {
            uuid: req.session.userId
        }
    });
    if (!findUser) {
        res.status(404).json({
            msg: "no data found"
        });
    }
    req.userId = findUser.id;
    req.role = findUser.role;

    next();
}

export const adminOnly = async(req, res, next) => {
    const findUser = await User.findOne({
        where: {
            uuid: req.session.userId
        }
    });
    if (!findUser) {
        res.status(404).json({
            msg: "no data found"
        });
    }
    if (findUser.role !== "admin") {
        return res.status(403).json({
            msg: "forbidden access you are not admin role"
        });
    }

    next();
}