import {Sequelize} from "sequelize";

const db = new Sequelize('nodeauth_db', 'root', '', {
    host: "localhost",
    dialect: "mysql",
});

export default db;