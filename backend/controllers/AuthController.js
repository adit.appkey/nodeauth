import User from "../models/UserModel.js";
import argon2 from "argon2";

//fitur login
export const login = async (req, res) => {
  //cari user
  console.log(res.body);
  const findUser = await User.findOne({
    where: {
      email: req.body.email,
    },
  });
  if (!findUser) {
    return res.status(404).json({
      msg: "no data found",
    });
  }
  //jika ada pake argon untuk verifikasi
  const match = await argon2.verify(findUser.password, req.body.password);
  if (!match) {
    return res.status(400).json({
      msg: "wrong password",
    });
  }
  //jika sudah match ambil session
  req.session.userId = findUser.uuid;
  //set data yg mau di passing
  const uuid = findUser.uuid;
  const name = findUser.name;
  const email = findUser.email;
  const role = findUser.role;
  res.status(200).json({
    uuid,
    name,
    email,
    role,
  });
  console.log(req.session);
};

export const currentUser = async (req, res) => {
  if (!req.session.userId) {
    return res.status(401).json({ msg: "please login to your account first" });
  }
  //cari user
  const findUser = await User.findOne({
    attributes: ["uuid", "name", "email", "role"],
    where: {
      uuid: req.session.userId,
    },
  });
  if (!findUser) {
    res.status(404).json({
      msg: "no data found",
    });
  }
  //end
  res.status(200).json(findUser);
};

//function logout
export const logout = (req, res) => {
  if (!req.session.userId) {
    return res.status(401).json({ msg: "no account login right now" });
  }
  req.session.destroy((err) => {
    if (err) return res.status(400).json({ msg: "can not logout" });
    res.status(200).json({ msg: "logout success" });
  });
};
