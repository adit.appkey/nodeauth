import User from "../models/UserModel.js";
import argon2 from "argon2";

export const getUsers = async(req, res) => {
    try {
        const response = await User.findAll({
            attributes:['uuid','name','email','role']
        });
        if (response === undefined || response.length == 0) {
            res.status(404).json({
                mesg: "no data found"
            });
        }
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
};

export const getUserById = async(req, res) => {
    try {
        const response = await User.findOne({
            attributes:['uuid','name','email','role'],
            where: {
                uuid: req.params.id
            }
        });
        if (!response) {
            return res.status(404).json({
                msg: "no data found"
            });
        }
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
};

export const createUser = async(req, res) => {
    const {name, email, password, confPassword, role} = req.body;
    if (password !== confPassword) return res.status(400).json({msg: "password and confirm password not match"})
    const hasPassword = await argon2.hash(password);
    try {
        const newUser = await User.create({
            name: name,
            email: email,
            password: hasPassword,
            role: role
        });
        res.status(201).json({
            msg: "success create user",
            data: newUser
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
        
    }
};

export const updateUser = async(req, res) => {
    const userFinded = await User.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!userFinded) {
        return res.status(404).json({
            msg: "no data found"
        });
    }
    const {name, email, password, confPassword, role} = req.body;
    let hasPassword;
    if (password === "" || password === null) {
        hasPassword: User.password
    }else{
        hasPassword = await argon2.hash(password);
    }
    if (password !== confPassword) return res.status(400).json({msg: "password and confirm password not match"})
    try {
        const newUser = await User.update({
            name: name,
            email: email,
            password: hasPassword,
            role: role
        }, {
            where: {
                id: userFinded.id
            }
        });
        console.log(newUser.name);
        res.status(200).json({
            msg: "User Updated Success",
            // data: finalData
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
        
    }
};
export const deleteUser = async(req, res) => {
    const userFinded = await User.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!userFinded) {
        return res.status(404).json({
            msg: "no data found"
        });
    }
    try {
        const newUser = await User.destroy({
            where: {
                id: userFinded.id
            }
        });
        res.status(200).json({
            msg: "User deleted Success",
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
        
    }
};