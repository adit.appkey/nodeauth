import Product from "../models/ProductModel.js";
import User from "../models/UserModel.js";
import { Op } from "sequelize";
import { response } from "express";

export const getProducts = async (req, res) => {
  try {
    let findProduct;
    if (req.role === "admin") {
      findProduct = await Product.findAll({
        attributes: ["uuid", "name", "price"],
        include: [
          {
            model: User,
            attributes: ["name", "email"],
          },
        ],
      });
    } else {
      findProduct = await Product.findAll({
        attributes: ["uuid", "name", "price"],
        where: {
          userId: req.userId,
        },
        include: [
          {
            model: User,
            attributes: ["name", "email"],
          },
        ],
      });
    }
    res.status(200).json(findProduct);
  } catch (error) {
    res.status(500).json({
      msg: error.message,
    });
  }
};

export const getProductById = async (req, res) => {
  try {
    const findProduct = await Product.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!findProduct)
      return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;
    if (req.role === "admin") {
      response = await Product.findOne({
        attributes: ["uuid", "name", "price"],
        where: {
          id: findProduct.id,
        },
        include: [
          {
            model: User,
            attributes: ["name", "email"],
          },
        ],
      });
    } else {
      response = await Product.findOne({
        attributes: ["uuid", "name", "price"],
        where: {
          [Op.and]: [{ id: findProduct.id }, { userId: req.userId }],
        },
        include: [
          {
            model: User,
            attributes: ["name", "email"],
          },
        ],
      });
    }
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createProduct = async (req, res) => {
  const { name, price } = req.body;
  try {
    const newProduct = await Product.create({
      name: name,
      price: price,
      userId: req.userId,
    });
    res
      .status(201)
      .json({ msg: "Product Created Successfuly", data: newProduct });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateProduct = async (req, res) => {
  try {
    const findProduct = await Product.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!findProduct) return res.status(404).json({ msg: "no data found" });
    const { name, price } = req.body;
    let response;
    if (req.role === "admin") {
      response = await findProduct.update(
        { name, price },
        {
          where: {
            id: findProduct.id,
          },
        }
      );
    } else {
      //jika user yg login tidak sama dengan user yg input product
      if (req.userId !== findProduct.userId) {
        return res.status(403).json({
          meg: "forbidden access different user account",
        });
      }
      response = await findProduct.update(
        { name, price },
        {
          where: {
            [Op.and]: [{ id: findProduct.id }, { userId: req.userId }],
          },
        }
      );
    }
    res.status(200).json({
      msg: "updated product successfully",
      data: response,
    });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const deleteProduct = async (req, res) => {
  try {
    const findProduct = await Product.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!findProduct) return res.status(404).json({ msg: "no data found" });
    const { name, price } = req.body;
    let response;
    if (req.role === "admin") {
      response = await findProduct.destroy({
        where: {
          id: findProduct.id,
        },
      });
    } else {
      //jika user yg login tidak sama dengan user yg input product
      if (req.userId !== findProduct.userId) {
        return res.status(403).json({
          meg: "forbidden access different user account",
        });
      }
      response = await findProduct.destroy({
        where: {
          [Op.and]: [{ id: findProduct.id }, { userId: req.userId }],
        },
      });
    }
    res.status(200).json({
      msg: "deleted product successfully",
    });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
