import express from "express";
import { currentUser, login, logout } from "../controllers/AuthController.js";

const router = express.Router(); //inisialisasi dulu

router.get('/profile', currentUser);
router.post('/login', login);
router.post('/logout', logout);

export default router;